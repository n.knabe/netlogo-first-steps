<!--
# SPDX-FileCopyrightText 2023 Carsten Lemmen
# SPDX-License-Identifier: CC0-1.0
# SPDX-FileContributor Carsten Lemmen <carsten.lemmen@leuphana.de
-->
# Netlogo first steps 2023

This repository serves students and teachers of the 2023 Ecosystem Modeling course as a playground for experimenting with `git`, with `markdown` and to exchange code.

# What to do


* Explore the content of this repository by looking through the tabs
* Edit this text file and change something trivial, save it and make your first commit
* Clone the repository to your local computer and look at the files you received

You may use the `GitHub Desktop` or `Visual Studio Code` client applications, or another one like `Tortoise Git` or install `git` in your command line, then perform one of the `clone` actions:

```
git clone https://gitlab.gwdg.de/leuphana-ecosystem-modeling-2023/netlogo-first-steps.git
```
# What to do next

* Have a look at the commit history
* Try to push/pull to and from this repository
* Readme again
